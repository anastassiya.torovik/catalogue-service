import os
import json
import ssl
from aiokafka import AIOKafkaConsumer
from motor.motor_asyncio import AsyncIOMotorClient

# Kafka credentials
BOOTSTRAP_SERVER = os.getenv('BOOTSTRAP_SERVER')
SECURITY_PROTOCOL = os.getenv('SECURITY_PROTOCOL')
USERNAME = os.getenv('USERNAME')
PASSWORD = os.getenv('PASSWORD')
SASL_MECHANISM = os.getenv('SASL_MECHANISM')
TOPIC = os.getenv('TOPIC')
API_AUTH = os.getenv('API_AUTH')
GROUP_ID = os.getenv('GROUP_ID', 'catalogue_group')

#Logger
LOG_LEVEL = str(os.getenv("LOG_LEVEL", "info"))

# Mongo
MONGO_DB_URI = os.getenv('MONGO_DB_URI', "mongodb://localhost:27017")
client = AsyncIOMotorClient(MONGO_DB_URI)
db = client.get_database("catalogue_db")
collection = db.get_collection("catalogue")
collection.create_index("_id", name="id_index")

# Kafka
consumer = AIOKafkaConsumer(
        TOPIC,
        bootstrap_servers=[BOOTSTRAP_SERVER],
        security_protocol=SECURITY_PROTOCOL,
        sasl_mechanism=SASL_MECHANISM,
        sasl_plain_username=USERNAME,
        sasl_plain_password=PASSWORD,
        auto_offset_reset="earliest",
        enable_auto_commit=True,
        value_deserializer=lambda x: json.loads(x.decode("utf-8")),
        key_deserializer=lambda x: x.decode("utf-8"),
        ssl_context=ssl.create_default_context()
    )

# API base url
BASE_URL = os.getenv('BASE_URL', 'http://127.0.0.1:5001')