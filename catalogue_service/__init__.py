from fastapi import FastAPI
from fastapi.responses import JSONResponse

import asyncio
import logging
import sys
from typing import Union
from hashlib import md5

from catalogue_service import config
from catalogue_service.config import consumer
from catalogue_service import functions

app = FastAPI()
app.state.mongo_client = config.client

logging.basicConfig(level=config.LOG_LEVEL.upper(), stream=sys.stdout)


@app.on_event("startup")
async def startup_event():
    asyncio.create_task(functions.consume())


@app.on_event("shutdown")
async def shutdown_event():
    await consumer.stop()


@app.get("/ping")
async def ping() -> dict:
    return {"Success": True}


@app.get("/")
async def root() -> dict:
    return {"message": "Welcome to catalogue service"}


@app.get("/get_records")
async def read_data(offset: int = 0, size: int = 20) -> list[dict]:
    """Read 'size' of catalogue records from mongo db starting from 'offset'"""
    documents = []
    cursor = config.collection.find({}).skip(offset)
    for document in await cursor.to_list(length=size):
        document["_id"] = str(document["_id"])
        documents.append(document)
    return documents


@app.get("/get_records_count")
async def count_data() -> dict:
    """Return total count of records in mongo"""
    records = {"count": await config.collection.count_documents({})}
    return records


@app.post("/create_product/{offer_id}", response_model=None)
async def create_product(offer_id: str) -> Union[dict, JSONResponse]:
    """Take an offset_id and retrieve its details from db. When offer is matched with another,
     create a product and check how many parameters they have in common and how many differ, store and return
      that information. If product already exists, just return it"""
    offer_detail = await config.collection.find_one({'_id': offer_id})
    if not offer_detail:
        return JSONResponse(status_code=404, content=f"Offer {offer_id} not found")

    if offer_detail.get('product'):
        return offer_detail['product']

    offer_params_hashed = {md5(str(key).lower().encode() + str(value).lower().encode()).hexdigest()
                           for key, value in offer_detail['parameters'].items()}
    offer_detail['product'] = {}

    for match_offer in offer_detail['matching_offers']:
        match_offer_params = (await config.collection.find_one({'_id': match_offer})).get('parameters')
        match_offer_params_hashed = {md5(str(key).lower().encode() + str(value).lower().encode()).hexdigest()
                                     for key, value in match_offer_params.items()}
        common_params_count = len(offer_params_hashed & match_offer_params_hashed)
        diff_params_count = len(offer_params_hashed ^ match_offer_params_hashed)
        offer_detail['product'][match_offer] = {'common_params_count': common_params_count,
                                                'different_params_count': diff_params_count}

        await config.collection.update_one({'_id': offer_id}, {"$set": offer_detail}, upsert=False)

    return offer_detail['product']



