from aiokafka import ConsumerRecord
import logging
import sys
import aiohttp
from aiohttp.client_reqrep import ClientResponse

from catalogue_service import config
from catalogue_service.config import consumer

logging.basicConfig(level=config.LOG_LEVEL.upper(), stream=sys.stdout)


def __bool__(self):
    """Adding method to aiohttp module to be able to use response as boolean object.
     Returns ``True`` if ``status`` is less than ``400``, ``False`` if not.
    """
    return self.ok


ClientResponse.__bool__ = __bool__


async def consume():
    await consumer.start()
    async with aiohttp.ClientSession() as session:
        try:
            async for msg in consumer:
                logging.info(f"Consumed msg with key: {msg.key}")
                msg = parse_msg(msg)
                msg = await extend_msg_with_data_from_api(msg, session)
                await config.collection.update_one({'_id': msg['_id']}, {"$set": msg}, upsert=True)
        finally:
            await consumer.stop()


def parse_msg(original_msg: ConsumerRecord) -> dict:
    """Restructure initial message and convert it to dict before it's stored in database"""
    msg = original_msg.value['payload']
    msg['key'] = original_msg.key
    msg['timestamp'] = original_msg.timestamp
    msg['type'] = original_msg.value['metadata']['type']
    msg['_id'] = msg['name'] if msg['type'] == 'category' else msg['id']
    return msg


async def get_data_from_api(offer_id: str, session: aiohttp.ClientSession) -> dict:
    url = f'{config.BASE_URL}/offer-matches/{offer_id}'
    headers = {'Auth': config.API_AUTH}
    response = await session.get(url, headers=headers, ssl=False)
    if response:
        resp_json = await response.json()
    else:
        resp_json = {}
    return resp_json


async def extend_msg_with_data_from_api(msg: dict, session: aiohttp.ClientSession) -> dict:
    if msg['type'] != 'offer':
        return msg
    offer_id = msg['id']
    api_resp = await get_data_from_api(offer_id, session)
    if api_resp:
        msg['matching_offers'] = [offer for offer in api_resp['matching_offers'] if offer != offer_id]
    return msg
