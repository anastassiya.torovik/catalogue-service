FROM python:3.10-slim

WORKDIR /app

ENV POETRY_HOME="/opt/poetry"
# prepend poetry to path
ENV PATH="$POETRY_HOME/bin:$PATH"

COPY ./catalogue_service /app/catalogue_service
COPY ./poetry.lock /app/
COPY ./pyproject.toml /app/


RUN pip install poetry
RUN poetry install
RUN poetry build

EXPOSE 8000
CMD ["poetry", "run", "uvicorn", "catalogue_service:app", "--host", "0.0.0.0", "--port", "8000"]