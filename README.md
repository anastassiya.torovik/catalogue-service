# catalogue-service

## Usage

You need to have Docker installed to use catalogue-service locally.
Clone the project. Once you are provided with .env file save it to catalogue-service directory.

Run the following command to start the app:

```
docker-compose up -d
```
Application will be running on 127.0.0.1:8082

If you want to stop the app run:
```
docker-compose stop
```

These commands will delete the app containers with volumes and will remove the image
```
docker-compose down -v
docker image rm catalogue-service_catalogue-service 
```
To get the data from database use endpoint GET \get_records
In parameters you can specify offset (default 0) and records size (default 20) to paginate the items.

Swagger documentation is auto-generated here: 127.0.0.1:8082/docs